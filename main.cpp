#include <QMainWindow>
#include <QApplication>
#include <QFileDialog>
#include <QPainter>

#include <QDebug>

#include "ui_preview.h"

int maxAlpha(int x, int y, const QImage & image, int window)
{
    int value = 0;
    for (int i = std::max(x - window, 0), maxI = std::min(x + window, image.width()); i < maxI; i++)  {
        for (int j = std::max(y - window, 0), maxJ = std::min(y + window, image.height()); j < maxJ; j++)
            value = std::max(value, image.pixelColor(i, j).alpha());
    }

    return value;
}

void underpaintImage(QLabel * target, const QImage & source)
{
    constexpr int windowSize = 5;
    constexpr QRgb baseColor = qRgb(0, 255, 0);

    QImage underlay(source.size(), source.format());

    for (int x = 0, width = underlay.width(); x < width; x++)  {
        for (int y = 0, height = underlay.height(); y < height; y++)  {
            QColor color(qRed(baseColor), qGreen(baseColor), qBlue(baseColor), maxAlpha(x, y, source, windowSize));
            underlay.setPixelColor(x, y, color);
        }
    }

    QRect region(0, 0, source.width(), source.height());

    QPainter painter(&underlay);
    painter.drawImage(region, source, region, Qt::NoOpaqueDetection | Qt::NoFormatConversion);

    target->setPixmap(QPixmap::fromImage(underlay));
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QMainWindow window;
    window.setCentralWidget(new QWidget());

    QMetaObject::invokeMethod(&window, [&window] () -> void  {
        Ui::Preview ui;
        ui.setupUi(window.centralWidget());

        QString fileName = QFileDialog::getOpenFileName(&window, QObject::tr("Select image to process"), QString(), QObject::tr("Images (*.png *.xpm *.jpg)"));
        if (fileName.isEmpty())  {
            window.close();
            return;
        }

        QImage image(fileName);
        if (image.isNull())  {
            window.close();
            return;
        }

        ui.original->setPixmap(QPixmap::fromImage(image));
        QMetaObject::invokeMethod(ui.processed, std::bind(underpaintImage, ui.processed, image), Qt::QueuedConnection);

        window.show();
    }, Qt::QueuedConnection);

    return QApplication::exec();
}
